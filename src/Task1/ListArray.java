package Task1;

import java.util.Arrays;

public class ListArray {
    private int[] inner;

    public ListArray() {
        inner = new int[] {};
    }

    public void addElement(int index, int value) {
        // Adding element in the end of array
        if (index > inner.length - 1) {
            inner = Arrays.copyOf(inner, inner.length + 1);
            inner[inner.length - 1] = value;
            return;
        }

        var newInner = new int[inner.length + 1];

        System.arraycopy(inner, 0, newInner, 0, index);
        newInner[index] = value;
        System.arraycopy(inner, index, newInner, index + 1, inner.length - index);

        inner = newInner;
    }

    public void removeElement(int index) {
        var newInner = new int[inner.length - 1];

        System.arraycopy(inner, 0, newInner, 0, index);
        System.arraycopy(inner, index + 1, newInner, index, inner.length - index - 1);

        inner = newInner;
    }

    public void replaceElement(int index, int value) {
        inner[index] = value;
    }

    @Override
    public String toString() {
        return Arrays.toString(inner);
    }
}
