package Task2;

import java.util.ArrayList;

public class ListNode
{
    private Node first;
    private Node last;

    public ListNode() {
        first = null;
        last = null;
    }

    public void addElement(int index, int value) {
        if (first == null) {
            first = new Node(null, value);
            return;
        }

        if (last == null) {
            if (index == 0) {
                last = first;
                first = new Node(last, value);
                return;
            }

            last = new Node(null, value);
            first.nextElement = last;
            return;
        }

        if (index <= 0) {
            first = new Node(first, value);
            return;
        }

        var current = first;
        int counter = 0;
        while (current.nextElement != null && counter < index - 1) {
            current = current.nextElement;
            counter += 1;
        }

        var nextElement = current.nextElement;
        current.nextElement = new Node(nextElement, value);
    }

    public void removeElement(int index) {
        if (index < 0) {
            return; // Or raise an exception
        }

        if (index == 0) {
            first = first.nextElement;
            return;
        }

        // Getting previous element
        var current = first;
        int counter = 0;
        while (counter < index - 1) {
            current = current.nextElement;
            counter += 1;
        }

        if (current.nextElement == null) {
            return; // Or raise an exception
        }
        var previous = current;

        // Getting element after our element (if exists)
        for (int i = 0; i < 2; i++) {
            current = current.nextElement;
            counter += 1;
        }

        previous.nextElement = current;
    }

    public void replace(int index, int value) {
        var current = first;

        for (int i = 0; i < index; i++) {
            current = current.nextElement;
        }

        current.value = value;
    }

    @Override
    public String toString() {
        if (first == null) {
            return "[]";
        }

        var array = new ArrayList<Integer>();

        var current = first;
        while (current.nextElement != null) {
            array.add(current.value);
            current = current.nextElement;
        }

        array.add(current.value);
        return array.toString();
    }
}
