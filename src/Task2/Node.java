package Task2;

public class Node {
    public Node nextElement;

    public int value;

    public Node(Node nextElement, int value) {
        this.nextElement = nextElement;
        this.value = value;
    }
}
