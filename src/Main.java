import Task1.ListArray;
import Task2.ListNode;

public class Main {
    public static void main(String[] args) {
        task1();
        task2();
    }

    public static void task1() {
        var array = new ListArray();
        array.addElement(0, 1234);
        array.addElement(10, 5678);

        System.out.println(array);

        array.addElement(0, 9999);
        array.addElement(1, 8888);

        System.out.println(array);

        array.removeElement(2);

        System.out.println(array);

        array.replaceElement(2, 228);

        System.out.println(array);

        array.removeElement(0);
        array.removeElement(0);
        array.removeElement(0);

        System.out.println(array);
    }

    public static void task2() {
        var array = new ListNode();
        array.addElement(10, 12345);
        array.addElement(10, 67890);
        array.addElement(1, 228);

        array.removeElement(2);

        array.replace(1, 9999);

        System.out.println(array);
    }
}